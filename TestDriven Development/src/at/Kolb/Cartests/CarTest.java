package at.Kolb.Cartests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import at.Kolb.Car.Car;
import at.Kolb.Car.Producer;

class CarTest {

	@Test
	void testGetPriceWithDiscount() {
		Producer p1 = new Producer(10,"Porsche");
		Car c1 = new Car("Gt3Rs",190000,p1);
		Assert.assertEquals(171000, c1.GetPriceWithDiscount());
	}
	
	@Test
	void testGetPriceWithDiscount1() {
		Producer f1 = new Producer(-10,"Ferarri");
		Car c2 = new Car("488",120000,f1);
		Assert.assertEquals(132000, c2.GetPriceWithDiscount());
	}
	@Test
	void testGetPriceWithDiscount2() {
		Producer v1 = new Producer(0,"VW");
		Car c3 = new Car("Golf 5",25000,v1);
		Assert.assertEquals(25000, c3.GetPriceWithDiscount());
	}
	


}
