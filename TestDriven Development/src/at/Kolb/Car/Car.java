package at.Kolb.Car;

public class Car {
	
	public String name;
	public int price;
	public Producer producer;
	

	public Car(String name, int price, Producer producer) {
		super();
		this.name = name;
		this.price = price;
		this.producer = producer;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	
	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

	public int GetPriceWithDiscount() {
		double discointoindecimal = this.producer.getDiscount() * 0.01;
		double multiplyfactor = 1.0 - discointoindecimal;
		
		double result;
		if (multiplyfactor > 0) {
			result = this.getPrice() * multiplyfactor;
		} else {
			result = this.getPrice();
		}
		return (int)result;
	}
}
	
