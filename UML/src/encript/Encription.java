package encript;

public class Encription implements Encripter {
	public int shifter;

	public Encription(int shifter) {
		super();
		this.shifter = shifter;

	}

	public Encription() {
		super();
		this.shifter = 2;
	}

	@Override
	public String decript(String data) {
		String result = "";

		for (int i = 0; i < data.length(); i++) {
			char character = data.charAt(i);
			int ascii = (int) character;
			char shift = (char) (ascii - shifter);
			if (ascii <= 122 && ascii >= 97 && shift < 97) {
				shift += 26;
				result += shift;
			} else if (ascii <= 90 && ascii >= 65 && shift < 65) {
				shift += 26;
				result += shift;

			} else {
				result += character;
			}
		}
		return result;
	}

	@Override
	public String encript(String data) {
		String result = "";
		for (int i = 0; i < data.length(); i++) {
			char character = data.charAt(i);
			int ascii = (int) character;
			char shift = (char) (ascii + shifter);
			if (ascii <= 122 && ascii >= 97 && shift > 122) {
				shift -= 26;
				result += shift;
			} else if (ascii <= 90 && ascii >= 65 && shift > 90) {
				shift -= 26;
				result += shift;

			} else {
				result += character;
			}

		}
		return result;
	}
}
