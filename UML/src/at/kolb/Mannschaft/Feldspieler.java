package at.kolb.Mannschaft;

import java.util.Date;

public class Feldspieler extends Fußballspieler {
	public int Geschwindigkeit;
	
	public Feldspieler(Date geburtsdatum, int id, int wert, String name,int Geschwindigkeit) {
		super(geburtsdatum, id, wert, name);
		this.Geschwindigkeit = Geschwindigkeit;
	}

	public int getGeschwindigkeit() {
		return Geschwindigkeit;
	}

	public void setGeschwindigkeit(int geschwindigkeit) {
		Geschwindigkeit = geschwindigkeit;
	}

	
}
