package at.kolb.Mannschaft;


import java.util.ArrayList;
import java.util.List;

public class Mannschaft {
	public int id;
	public String name;
	public Fußballspieler fußballpieler;
	public List<Fußballspieler> players;
	public int wert;
	
	public Mannschaft(int id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.players = new ArrayList<Fußballspieler>();
	}
	
	public void playerAdd(Fußballspieler player){
		players.add(player);
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getValueOfTeam() {
	int gesamtwert = 0;
	for (Fußballspieler fußballspieler : players) {
		gesamtwert += fußballspieler.getWert();
	}
	System.out.println(gesamtwert);
	return gesamtwert;
	}

}
