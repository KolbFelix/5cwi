package at.kolb.Mannschaft;

import java.util.Date;

public class Main {

	public static void main(String[] args) {
		Fußballspieler f1 = new Fußballspieler(new Date(), 1, 12000000, "Ronaldo");
		Fußballspieler f2 = new Fußballspieler(new Date(),2, 5000000, "Dibala");
		Fußballspieler f3 = new Fußballspieler(new Date(), 3, 1200000, "Chielinie");
		Tormann t1 = new Tormann(new Date(),4,8000000,"ter stegen",0.2);
		Mannschaft m1 = new Mannschaft(1, "Juve");
		
		m1.playerAdd(f2);
		m1.playerAdd(f1);
		m1.playerAdd(f3);
		m1.playerAdd(t1);
		m1.getValueOfTeam();
		

	}

}
