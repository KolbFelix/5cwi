package at.kolb.Fertilize;

import java.util.ArrayList;
import java.util.List;

public class Area {
	private int size;
	public List<Tree> trees;
	
	public Area(int size) {
		super();
		this.size = size;
		this.trees = new ArrayList<Tree>();
	}
	
	
	public int getSize() {
		return size;
	}


	public void setSize(int size) {
		this.size = size;
	}


	public void addTree(Tree trees) {
		this.trees.add(trees);
	}
	
	public void FertilizeAll() {
		for (Tree tree : trees) {
			tree.getStrategy().doFertilize();;
		}	
	}
	
	
}
