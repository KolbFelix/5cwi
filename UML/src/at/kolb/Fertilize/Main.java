package at.kolb.Fertilize;

public class Main {

	public static void main(String[] args) {
		Tree t1 = new Connifer(1,new TopGrow());
		Tree t2 = new Connifer(1,new SuperGrow());
		Tree t3 = new LeafTree(1,new TopGrow());
		
		Area a = new Area(100000);
		
		a.addTree(t1);
		a.addTree(t2);
		a.addTree(t3);
		
		
		a.FertilizeAll();
	}

}
