package at.kolb.Playable;

public class Main {

	public static void main(String[] args) {
		Player p1 = new Player();
		CD c1 = new CD("1");
		DVD dvd1 = new DVD("1");
		Title t1 = new Title("Titanic");
		Title t2 = new Title("Mission Impossible 1");
		Title t3 = new Title("Fast&Furious");
		Song s1 = new Song("Africa");
		
		
		dvd1.addTitle(t1);
		dvd1.addTitle(t2);
		dvd1.addTitle(t3);
		c1.addTitle(s1);
		p1.addPlayable(t1);
		p1.addPlayable(t2);
		p1.addPlayable(t3);
		p1.addPlayable(s1);
		p1.playAll();
	}

}
