package at.kolb.Playable;

import java.util.ArrayList;
import java.util.List;

public class DVD {

	private String name;
	public List<Title>titles;

	public DVD(String name) {
		super();
		this.name = name;
		this.titles = new ArrayList<Title>();

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public void addTitle(Title title) {
		this.titles.add(title);
	}
	
	
}
