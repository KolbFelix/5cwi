package at.kolb.CashMachine;

import java.util.Scanner;

public class CashMachine {
	Withdraw w1 = new Withdraw();
	Deposit d1 = new Deposit();
	int Kontostand = 0;
	Scanner action = new Scanner(System.in);

	public void action() {
		while (true) {
			int a = action.nextInt();
			if (a == 1) {
				w1.transaction();
				Kontostand -= w1.getWithdrawed();
			}
			if (a == 2) {
				d1.transaction();
				Kontostand += d1.getDeposited();
			}
			if (a == 3) {
				System.out.println("Ihr Kontostand betr�gt " + Kontostand + "�");
			}
			if (a == 4) {
				System.out.println("Bankomat beendet");
				System.exit(0);
			}
		}
	}
}
