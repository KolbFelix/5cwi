package at.kolb.factorypattern;

public class Snowflake implements Actor {

	@Override
	public void move() {
		System.out.println("Snowflake is moving");
	}

	@Override
	public void render() {
		System.out.println("Snowflake generatet");
	}

}
