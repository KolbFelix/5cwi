package at.kolb.factorypattern;

public class Homer implements Actor {

	@Override
	public void move() {
		System.out.println("Homer is moving");		
	}

	@Override
	public void render() {
		System.out.println("Homer rendered");		
		
	}

}
