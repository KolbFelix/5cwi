package at.kolb.factorypattern;

import java.util.Random;

public class main {

	public static void main(String[] args) {
		Game g1 = new Game();
		Snowflake sf1 = new Snowflake();
		Homer hom1 = new Homer();
		ActorFactory rf1 = new ActorFactory();
		
		
		g1.addActor(hom1);
		g1.addActor(sf1);
		
		g1.renderAll();
		g1.moveAll();
		
		rf1.createActor();
		g1.addActor(rf1.createActor());
		g1.renderAll();
	}

}
