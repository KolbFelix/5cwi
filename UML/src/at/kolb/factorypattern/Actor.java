package at.kolb.factorypattern;

public interface Actor {
	public void move();
	public void render();
}
