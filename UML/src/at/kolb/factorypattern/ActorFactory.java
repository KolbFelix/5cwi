package at.kolb.factorypattern;

import java.util.Random;

public class ActorFactory {

	public Actor createActor() {
		int rndm;
		Actor newActor = null;
		rndm = (int) (Math.random()*((3-1)+1))+1;
		if (rndm == 1) {
			newActor = new Snowflake();
		}
		else if (rndm == 2) {
			newActor = new Homer();
		}
		return newActor;
	}
}
