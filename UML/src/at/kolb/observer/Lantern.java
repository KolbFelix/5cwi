package at.kolb.observer;

public class Lantern implements Observable {
	
	@Override
	public void inform() {
		System.out.println("It's dark now (Lantern)");		
		
	}

}
