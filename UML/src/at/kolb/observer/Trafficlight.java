package at.kolb.observer;

public class Trafficlight implements Observable {

	@Override
	public void inform() {
		System.out.println("It's dark now (Trafficlight)");		
	}

}
