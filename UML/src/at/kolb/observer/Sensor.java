package at.kolb.observer;

import java.util.ArrayList;
import java.util.List;


public class Sensor {
	public List<Observable> observables;

	public Sensor() {
		super();
		this.observables = new ArrayList<Observable>();
	}
	
	public void addObservable(Observable observables) {
	this.observables.add(observables);
	}
	
	public void ItsDark() {
		for (Observable observable : observables) {
			observable.inform();
		}	
	}
}
