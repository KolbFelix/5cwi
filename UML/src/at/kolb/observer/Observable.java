package at.kolb.observer;

public interface Observable {

	public void inform();
}
